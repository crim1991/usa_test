<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Test</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<style>
		th {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<table class="table table-bordered">
					<thead>
					<tr>
						<th id="age" sort="SORT_ASC">Age</th>
						<th id="eyeColor" sort="SORT_ASC">Eye Color</th>
						<th id="name" sort="SORT_ASC">Name</th>
						<th id="gender" sort="SORT_ASC">Gender</th>
						<th id="company" sort="SORT_ASC">Company</th>
						<th id="email" sort="SORT_ASC">Email</th>
						<th id="phone" sort="SORT_ASC">Phone</th>
						<th id="address" sort="SORT_ASC">Address</th>
					</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div class="text-center">
					<button class="read_btn btn btn-primary">Read more</button>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('main.js') }}"></script>
</body>
</html>