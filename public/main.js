$(function () {
	//GET DATA FROM SERVER
	$.ajax({
		url: '/get-first-contacts',
		method: 'GET',
	}).done(function (result) {
		var contacts = JSON.parse(result);
		$.each(contacts, function (key, item) {
			contactRow(item);
		});
	});

	//Read more
	$('.read_btn').on('click', function () {
		//get last table id
		let last_id = $('.table tbody').find('tr:last-child').attr('id');
		let $this = $(this);

		$.ajax({
			url: '/get-next-contacts/' + last_id,
			method: 'GET',
		}).done(function (result) {
			var contacts = JSON.parse(result);
			$.each(contacts, function (key, item) {
				contactRow(item);
			});
			var table_contacts = $('.table tbody').find('tr').length;
			let active_contacts = $.ajax({
				url: '/get-active-contacts',
				method: 'GET',
				async: false
			}).responseText;
			if (active_contacts == table_contacts) {
				$this.addClass('d-none');
			}
		});
	});


	function contactRow(item) {
		$('.table').find('tbody')
			.append(
				'<tr id="'+item.id+'">' +
				'<td>' + item.age + '</td>' +
				'<td>' + item.eyeColor + '</td>' +
				'<td>' + item.name + '</td>' +
				'<td>' + item.gender + '</td>' +
				'<td>' + item.email + '</td>' +
				'<td>' + item.company + '</td>' +
				'<td>' + item.phone + '</td>' +
				'<td>' + item.address + '</td>' +
				'</tr>'
			);
	}

	$('table tr th').on('click', function () {
		let sort_type = $(this).attr('id');
		let involved_items = $('.table tbody').find('tr');
		let ids = [];
		let mode = $(this).attr('sort');
		let $this = $(this);
		
		$.each(involved_items, function (index, value) {
			ids.push($(value).attr('id'));
		});

		$.ajax({
			url: '/sort-by-type/' + sort_type + '/' + ids + '/' + mode,
			method: 'GET',
		}).done(function (result) {
			var sorted_contacts = JSON.parse(result);
			$('.table tbody').html('');

			$.each(sorted_contacts, function (key, item) {
				contactRow(item);
			});
			if (mode == 'SORT_ASC') {
				$this.attr('sort', 'SORT_DESC');
			} else {
				$this.attr('sort', 'SORT_ASC');
			}
		});
	});
});