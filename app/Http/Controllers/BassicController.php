<?php

namespace App\Http\Controllers;

use App\Cities;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class BassicController extends Controller
{
	public function index()
	{
		return view('index');
	}

	public function getContacts()
	{
		$contacts = '[
		  {
			"item": [
			  {
				"id": 0,
				"isActive": true,
				"age": 35,
				"eyeColor": "brown",
				"name": "Isabel Noel",
				"gender": "female",
				"company": "RUBADUB",
				"email": "isabelnoel@rubadub.com",
				"phone": "+1 (905) 515-2287",
				"address": "353 Vanderveer Place, Welda, Texas, 3873"
			  }
			]
		  },
		  {
			"item": [
			  {
				"id": 1,
				"isActive": true,
				"age": 28,
				"eyeColor": "green",
				"name": "Campos Pittman",
				"gender": "male",
				"company": "EVENTIX",
				"email": "campospittman@eventix.com",
				"phone": "+1 (847) 543-3337",
				"address": "860 Ralph Avenue, Yettem, Louisiana, 726"
			  }
			]
		  },
		  {
			"item": [
			  {
				"id": 2,
				"isActive": true,
				"age": 28,
				"eyeColor": "brown",
				"name": "Sharpe French",
				"gender": "male",
				"company": "QUAREX",
				"email": "sharpefrench@quarex.com",
				"phone": "+1 (869) 472-3624",
				"address": "131 Hinsdale Street, Gasquet, Federated States Of Micronesia, 5052"
			  }
			]
		  },
		  {
			"item": [
			  {
				"id": 3,
				"isActive": false,
				"age": 31,
				"eyeColor": "brown",
				"name": "Teresa Bush",
				"gender": "female",
				"company": "OVIUM",
				"email": "teresabush@ovium.com",
				"phone": "+1 (957) 433-3564",
				"address": "257 Williams Place, Elizaville, North Carolina, 2007"
			  }
			]
		  },
		  {
			"item": [
			  {
				"id": 4,
				"isActive": true,
				"age": 33,
				"eyeColor": "green",
				"name": "Lowe Horne",
				"gender": "male",
				"company": "BARKARAMA",
				"email": "lowehorne@barkarama.com",
				"phone": "+1 (967) 560-3576",
				"address": "894 Amboy Street, Skyland, Connecticut, 156"
			  }
			]
		  },
		  {
			"item": [
			  {
				"id": 5,
				"isActive": true,
				"age": 21,
				"eyeColor": "blue",
				"name": "Minnie Reese",
				"gender": "female",
				"company": "EPLODE",
				"email": "minniereese@eplode.com",
				"phone": "+1 (855) 481-3494",
				"address": "703 Prospect Place, Wilmington, Indiana, 1827"
			  }
			]
		  }
		]';

		return $this->cleanArray($contacts);
	}

	public function getFirstTwoContacts()
	{
		$contacts = $this->getContacts();
		$counter = 0;
		$first_contacts = array();

		foreach ($contacts as $key => $value) {
			if ($counter < 2) {
				$counter++;
				if ($value['isActive'] == 'true') {
					$first_contacts[] = $value;
				}
			}
		}
		return json_encode($first_contacts);
	}

	public function getNextContacts($last_contact_id, $amount = 2)
	{
		$contacts = $this->getContacts();
		$counter = 0;
		$new_contacts = array();
		foreach ($contacts as $key => $value) {
			if ($value['id'] > $last_contact_id && $counter < $amount) {
				if ($value['isActive'] == 'true') {
					$new_contacts[] = $value;
					$counter++;
				}
			}
		}
		return json_encode($new_contacts);
	}

	public function getActiveContactsNr()
	{
		$contacts = $this->getContacts();
		$new_contacts = array();
		foreach ($contacts as $key => $value) {
			if ($value['isActive'] == 'true') {
				$new_contacts[] = $value;
			}
		}
		return count($new_contacts);
	}

	public function getContactsByIds($ids)
	{
		$contacts = $this->getContacts();
		$sorted_contacts = array();
		foreach ($contacts as $key => $value) {
			if (in_array($value['id'], $ids)) {
				$sorted_contacts[] = $value;
			}
		}

		return $sorted_contacts;
	}

	public function sortByType($type, $ids, $mode)
	{
		$ids = explode(',', $ids);
		$contacts = $this->getContactsByIds($ids);
		$new_array = $this->array_sort($contacts, $type, $mode);

		return json_encode($new_array);
	}

	public function cleanArray($array)
	{
		$array = json_decode($array, true);
		$clean_array = array();
		foreach ($array as $key => $contact) {
			$clean_array[] = $contact['item'][0];
		}
		return $clean_array;
	}

	function array_sort($array, $on, $order = 'SORT_ASC')
	{
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case 'SORT_ASC':
					asort($sortable_array);
					break;
				case 'SORT_DESC':
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[] = $array[$k];
			}
		}

		return $new_array;
	}
}
