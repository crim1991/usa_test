<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BassicController@index');

Route::get('/get-first-contacts', 'BassicController@getFirstTwoContacts');
Route::get('/get-next-contacts/{last_id}', 'BassicController@getNextContacts');
Route::get('/get-active-contacts', 'BassicController@getActiveContactsNr');
Route::get('/sort-by-type/{type}/{ids}/{mode}', 'BassicController@sortByType');
